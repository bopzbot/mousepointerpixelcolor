// getscreenpixel.cpp

#include <iostream>
#include <windows.h>
#include <winuser.h> 

POINT mousePos = {0,0};
LPPOINT mouseCords = &mousePos;

int main()
{
    const HDC displayHandle = GetDC(NULL);

    while (true) {
        GetCursorPos(mouseCords);
        std::cout << "MouseCoords: x = " << mouseCords->x << ", y = " << mouseCords->y << "\n";
        std::cout << "colors = " << std::hex << GetPixel(displayHandle, mouseCords->x, mouseCords->y) << std::dec << "\n";
        Sleep(200);
        std::cout << "\033[2J\033[1;1H";
    }
}